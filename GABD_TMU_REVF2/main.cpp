#include <Arduino.h>
#include <Adafruit_SleepyDog.h>

//	REV H
//	5/22/18
//  MOST RECENTLY LOADED ON ALL TMU'S
//	Liam Gallagher (LMG)

const int relayPin[4] = {7, 6, 5, 4}; // sets relay channels 1 --> 4
bool relayStat[4];

byte ib = 0;   // for incoming Serial1 data

byte high;
byte low;

float allTemps[12];		//OUTPUT GIVING CUSTOMER ALL TEMPERATURES
int outputTemps[12];
int tcal[16];			//T_CAL THAT THE LAST GUY PUT IN...CURRENTLY NOT USED
String val;
String setCmd;
String hwasCmd;
byte instr[1];

//INITIAL DECLARATION OF ALL PUBLIC FUNCTIONS, IN THE FUTURE SHOULD GO TO SEPERATE .c || .h files
void startUp();
void convTemps(int tchan);
void readTemps();
void setActive();
void relayStatus();
void hwasReset();

void setup() {
	// Configure digital pins for HWAS and limit switches
	pinMode(22, INPUT); 			// digital input to real HWAS INV EVENT signal
	pinMode(23, OUTPUT); 			// HWAS reset signal

	// set the relay control digital pins as output:
	for (int i = 0; i<4; i++) {
		pinMode(relayPin[i], OUTPUT);
		digitalWrite(relayPin[i], LOW);
		relayStat[i] = false;
	}
	for (int i = 0; i<12 ;i++){
		outputTemps[i]=0;
	}
	for (int i = 0; i<12 ;i++){
		tcal[i]=0;
	}
	//START Serial1 COMMUNICATION AT 115200 BAUD RATE
	//This is communication to the host
	Serial1.begin(115200);
	//START Serial1 COMMUNICATION AT 115200 BAUD RATE
	//This is communication to the COMPUTER USED FOR DEBUG
	//	Serial.begin(115200);
	//Uncomment to ENABLE Watchdog timer
	int countdownMS = Watchdog.enable(8000);

}
void loop() {

	ib = 0x00;

	if (Serial1.available() > 0) {
		Serial1.setTimeout(9);			//SET TO 9ms IN ORDER TO TO PING AT 10ms AND ACHIEVE 100Hz COMM RATE.
		//	 read the incoming byte:
		ib = Serial1.read();
		//		Serial.println(ib,BIN);

		startUp();

		//		delay(100);

	}
	if (Serial.available() > 0) {
		//	 read the incoming byte:
		ib = Serial.read();
		Serial.println(ib,BIN);

		startUp();

		//		delay(100);

	}
}
void startUp(){

	if (!bitRead(ib,5) && !bitRead(ib,6) && bitRead(ib,7)){ //Looks for 100XXXX, must have the 100 to proceed
		//		Serial1.write(255);
		Serial1.write(127);
		Serial1.write(63);
		Serial1.write(31);
		setActive();
	}
}
void setActive(){

	// Below looks at the first 4-bits and IF == 1, turn heater ON, IF == 0, turn heater OFF
	for (int i = 0;i<4;i++){
		if (bitRead(ib,i)){
			digitalWrite(relayPin[i], HIGH);
			relayStat[i] = true;
		}
		else {
			digitalWrite(relayPin[i], LOW);
			relayStat[i] = false;
		}
	}
	// Look at fourth bit and IF == 1, enable HWAS_RESET
	if (bitRead(ib,4)){
		hwasReset();
	}
	readTemps();		// Output the temperatures
	relayStatus();
	Watchdog.reset();	// RESET Watchdog

}
void readTemps(){

	for (int i = 0;i<12;i++){
		convTemps(i);
		//		allTemps[i] = allTemps[i]*10;

		outputTemps[i] = (int) allTemps[i];

		//		Serial.println(outputTemps[i]);
		Serial1.write(outputTemps[i]);

	}
	//	for (int i = 0;i<12;i++){
	//		//		Serial1.write(highByte(outputTemps[i]));
	//		//		Serial1.write(lowByte(outputTemps[i]));
	//
	//		Serial1.write(outputTemps[i]);
	//
	//
	//		//		Serial.write(highByte(outputTemps[i]));
	//		//		Serial.write(lowByte(outputTemps[i]));
	//		//		Serial.println("");
	//		//		Serial1.write("test");
	//
	//	}
}
void relayStatus(){

	float number = 0.5;

	for (int i = 0;i<4;i++){
		if (relayStat[i]){
			number += pow(2,i);
		}
	}
	if (digitalRead(22) == 1){
		number += pow(2,4);
	}

	Serial1.write((int) number);

}
void hwasReset(){
	//This function resets the HWAS which requires
	//digital pin 23 goes HIGH for 1 second, the turns off
	digitalWrite(23,HIGH);
	delay(1000);
	digitalWrite(23,LOW);
	//	Serial1.println("HWAS RESET");		//This statement was put in for test and debug
}
void convTemps(int tchan) { // convert A/D counts to corrected temperature DEGREE C
	//Happens twice because sometimes the first output is junk, while the second give valid data
	allTemps[tchan] = (((0.0049*float(analogRead(tchan)))-2.6632)/0.0261);//+tcal[tchan];
	allTemps[tchan] = (((0.0049*float(analogRead(tchan)))-2.6632)/0.0261);//+tcal[tchan];
}
