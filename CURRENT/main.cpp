#include <Arduino.h>
#include <Adafruit_SleepyDog.h>

//	REV B
//	3/7/18
//	Liam Gallagher

const int relayPin[4] = {7, 6, 5, 4}; // sets relay channels 1 --> 4
bool relayStat[4];

byte ib = 0;   // for incoming Serial1 data

float allTemps[16];		//OUTPUT GIVING CUSTOMER ALL TEMPERATURES
float tcal[16];			//T_CAL THAT THE LAST GUY PUT IN...CURRENTLY NOT USED
String val;
String setCmd;
String hwasCmd;
byte instr[1];

void convTemps(int tchan);
void readTemps();
void setActive();
void hwasReset();

void setup() {

	// Configure digital pins for HWAS and limit switches
	pinMode(22, INPUT); 			// digital input to real HWAS INV EVENT signal
	pinMode(23, OUTPUT); 			// HWAS reset signal

	// set the relay control digital pins as output:
	for (int i = 0; i<4; i++) {
		pinMode(relayPin[i], OUTPUT);
		digitalWrite(relayPin[i], LOW);
		relayStat[i] = false;
	}

	for (int i = 0; i<12 ;i++){
		allTemps[i]=0;
	}
	for (int i = 0; i<12 ;i++){
		tcal[i]=0;
	}

	//START Serial11 COMMUNICATION AT 115200 BAUD RATE
	//This is communication to the host
	Serial1.begin(115200);

	//START Serial1 COMMUNICATION AT 115200 BAUD RATE
	//This is communication to the COMPUTER USED FOR DEBUG
//	Serial1.begin(115200);

	//Uncomment to ENABLE Watchdog timer
	//	int countdownMS = Watchdog.enable(8000);

}
void loop() {

	ib = 0x00;

	if (Serial1.available() > 0) {
		Serial1.setTimeout(9);			//SET TO 9ms IN ORDER TO TO PING AT 10ms AND ACHIEVE 100Hz COMM RATE.
		// read the incoming byte:
		ib = Serial1.read();

//		Serial1.println(ib,BIN);   //prints instruction to COMPUTER SCREEN NOT SPEEDGOAT!!!

		if (!bitRead(ib,5) && !bitRead(ib,6) && !bitRead(ib,7)){
			setActive();
		}
	}
}
void setActive(){

	for (int i = 0;i<4;i++){
		if (bitRead(ib,i)){
			digitalWrite(relayPin[i], HIGH);
			relayStat[i] = true;
		}
		else {
			digitalWrite(relayPin[i], LOW);
			relayStat[i] = false;
		}
	}
	if (bitRead(ib,4)){
		hwasReset();
	}
	readTemps();
	Watchdog.reset();
}
void readTemps(){

	for (int i = 0;i<12;i++){
		convTemps(i);
		Serial1.print(allTemps[i],1);
		Serial1.print(",");
	}
	for (int i = 0;i<4;i++){
		Serial1.print(relayStat[i]);
		Serial1.print(",");
	}
	Serial1.print(digitalRead(22)); //READS HWAS
	Serial1.print(",");
}
void hwasReset(){
	digitalWrite(23,HIGH);
	delay(1000);
	digitalWrite(23,LOW);
//	Serial1.println("HWAS RESET");		//This statement was put in for test and debug
}
void convTemps(int tchan) { // convert a/d counts to corrected temperature deg C
	allTemps[tchan] = (((0.0049*float(analogRead(tchan)))-2.6632)/0.0261);//+tcal[tchan];
	allTemps[tchan] = (((0.0049*float(analogRead(tchan)))-2.6632)/0.0261);//+tcal[tchan];
}


