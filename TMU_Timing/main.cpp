#include <Arduino.h>
#include <Adafruit_SleepyDog.h>

const int relayPin[4] = {7, 6, 5, 4}; // sets relay channels 1 --> 4

bool relayStat[4];
int ib = 0;   // for incoming Serial data

float allTemps[16];		//OUTPUT GIVING CUSTOMER ALL TEMPERATURES
float tcal[16];			//T_CAL THAT THE LAST GUY PUT IN...CURRENTLY NOT USED
String val;
String setCmd;
String hwasCmd;
String instr;

unsigned long inputStart = 0;
unsigned long startTime = 0;

unsigned long readStart = 0;
unsigned long readEnd = 0;
unsigned long readTime = 0;


unsigned long sendStart = 0;
unsigned long sendEnd = 0;


void convTemps(int tchan);
void readTemps();
void setActive();
void hwasReset();

void setup() {

	// Configure digital pins for HWAS and limit switches
	pinMode(22, INPUT); 			// digital input to real HWAS INV EVENT signal
	pinMode(23, OUTPUT); 			// HWAS reset signal

	// set the relay control digital pins as output:
	for (int i = 0; i<4; i++) {
		pinMode(relayPin[i], OUTPUT);
		digitalWrite(relayPin[i], LOW);
		relayStat[i] = false;
	}

	for (int i = 0; i<12 ;i++){
		allTemps[i]=0;
	}
	for (int i = 0; i<12 ;i++){
		tcal[i]=0;
	}

	//START Serial COMMUNICATION AT 115200 BAUD RATE
	Serial.begin(115200);

	//	int countdownMS = Watchdog.enable(8000);

//			Serial.println("Starting...");

	//		delay(100);

	//	Serial.println("TMU Has started...");
	//	Serial.println("Press 'R' to read temps.");
	//	Serial.println("Press 'SXXXX' TO TURN ON HEATERS");

}
void loop() {

	if (Serial.available() > 0) {
		Serial.setTimeout(9);			//SET TO 9ms IN ORDER TO TO PING AT 10ms AND ACHIEVE 100Hz COMM RATE.
		// read the incoming byte:
		//		ib = Serial.read();

		instr = Serial.readStringUntil(char(13));
		inputStart = micros();

		setCmd = instr.substring(0,1);
		hwasCmd = instr.substring(5,6);

//		Serial.println(instr);

		if (setCmd == "S" && hwasCmd == "H"){ 		//SXXXXHX ex...S0000H0

			readStart = micros();

//			Serial.println(readStart);

			setActive();

			readEnd = micros();

			Watchdog.reset();

//
//			Serial.println(readStart-inputStart);
//			Serial.println(readEnd-readStart);


		}
		else return;
	}

}
void setActive(){
	for (int i = 1;i<5;i++){
		if (instr.substring(i,i+1).equals("1")){
			digitalWrite(relayPin[i-1], HIGH);
			relayStat[i-1] = true;
		}
		else {
			digitalWrite(relayPin[i-1], LOW);
			relayStat[i-1] = false;
		}
	}
	if (instr.substring(6,7).equals("1")){
		hwasReset();
	}
	readTemps();
}
void readTemps(){
	//	Serial.println("Reading Temperatures...");
//	readStart = micros()-inputStart;
	for (int i = 0;i<12;i++){
		convTemps(i);
		Serial.println(micros()-inputStart);
	}
	for (int i = 0;i<4;i++){
//		Serial.println(relayStat[i]);
		Serial.println(micros()-inputStart);
	}
//	Serial.println(digitalRead(22)); //READS HWAS
	Serial.println(micros()-inputStart);
}
void hwasReset(){
	digitalWrite(23,HIGH);
	delay(1000);
	digitalWrite(23,LOW);
}
void convTemps(int tchan) { // convert a/d counts to corrected temperature deg C
	allTemps[tchan] = (((0.0049*float(analogRead(tchan)))-2.6632)/0.0261)+tcal[tchan];
}


