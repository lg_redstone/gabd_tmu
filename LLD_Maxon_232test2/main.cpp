#include <Arduino.h>

int lowByte = 0;   // for incoming Serial11 data


void DLE_STX();
void enable();
void disable();
void profileVelocity();
void profileAcceleration();
void profileDeceleration();
void targetPosition();
void execute();
void setPositionProfileMode();

void setup() {

	Serial1.begin(115200);
	Serial.begin(115200);
	//	delay(10);
	//	enable();			//working DON'T TOUCH!
	//	delay(10);
	//	setPositionProfileMode();
	delay(10);
	targetPosition();
	//	delay(10);


	//	profileVelocity();
	//	profileAcceleration();
	//	profileDeceleration();
}
void loop() {
	//	enable();
	//	setPositionProfileMode();
	//	targetPosition();
	//	delay(5000);

}

void enable(){

	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x40);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x06);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x22);
	Serial1.write(0x99);
	delay(5);
	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x40);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x0F);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xB3);
	Serial1.write(0x07);

}
void disable(){

	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x40);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x82);
	Serial1.write(0x2B);
}
void DLE_STX(){
	Serial1.write(0x90);
	Serial1.write(0x02);
}
void profileVelocity(){

	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x81);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0xE8);
	Serial1.write(0x03);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x4C);
	Serial1.write(0xF5);

}
void profileAcceleration(){

	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x83);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x10);
	Serial1.write(0x27);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xA6);
	Serial1.write(0xCD);

}
void profileDeceleration(){
	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x84);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x10);
	Serial1.write(0x27);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xBE);
	Serial1.write(0x0A);
}
void targetPosition(){

	int waitTime = 2500;

	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x7A);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0xE8);
	Serial1.write(0x03);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x6E);
	Serial1.write(0x6E);

	delayMicroseconds(waitTime);
	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x40);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x7F);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xBB);
	Serial1.write(0xDF);

	if (Serial1.available()) {
		// read the incoming byte:
		while(Serial1.available()>0){
			lowByte = Serial1.read();
			Serial.println(lowByte,HEX);
		}
	}

	delayMicroseconds(waitTime);
	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x40);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x0F);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xB3);
	Serial1.write(0x07);

}
void execute(){

	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);

	Serial1.write(0x84);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x10);
	Serial1.write(0x27);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xBE);
	Serial1.write(0x0A);
}

void setPositionProfileMode(){
	DLE_STX();
	Serial1.write(0x68);
	Serial1.write(0x04);
	Serial1.write(0x01);
	Serial1.write(0x60);
	Serial1.write(0x60);
	Serial1.write(0x00);
	Serial1.write(0x01);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(0xDA);
	Serial1.write(0x67);

}

