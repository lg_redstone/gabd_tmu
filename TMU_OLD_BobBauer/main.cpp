#include <Arduino.h>

const int RlyPin[4] = {7, 6, 5, 4}; // sets relay channels 1 --> 4

// delcare global variabls

boolean htrstat[4] = {false, false,false,false}; // heater loop enable
float htrtemp[4] = {0.0, 0.0, 0.0, 0.0}; // heater loop set point
float temp[12] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,0.0, 0.0, 0.0, 0.0}; // corrected temperature deg C
float tcal[12] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,0.0, 0.0, 0.0, 0.0}; // correction values

void ProcTemp(int tc);
void convtemp(int tchan);

void setup() {
  int i = 0;
  // put your setup code here, to run once:

  // set the relay control digital pins as output:
  for (i = 0; i<4; i++) {
    pinMode(RlyPin[i], OUTPUT);
  }

  // Configure digital pins for HWAS and limit switches

  pinMode(22, INPUT); // digital input to real HWAS INV EVENT signal
  pinMode(23, OUTPUT); // HWAS reset signal
  pinMode(24, INPUT_PULLUP); // LIMIT SWITCH 1 input, inverted level
  pinMode(25, INPUT_PULLUP); // LIMIT SWITCH 2 input, inverted level


  // Initialize all relays off
  for (i = 0; i<4; i++) {
  digitalWrite(RlyPin[i], LOW);
  }
  // Initializ serial port 1
  Serial1.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  float value;
  int tlen = 0;
  String instr;
  String cmd;
  String val;
  int i;
  int level;

  for (i=0; i < 12; i++) { // read all 12 temp sensors
    convtemp(i); // set temp array to current temperatures
    delay(10); // for stability
  };

  for (i=0; i < 4; i++) { // process 4 temperature loops

      ProcTemp(i); // process the temperature channel
    }

  if(Serial1.available() > 0) { // is there data in the serial buffer?
    Serial1.setTimeout(1000); // allow time for data transfer to finish
    instr = Serial1.readStringUntil(char(13)); // read until a CR
    tlen = instr.length(); // determine string length

    if (tlen > 2) { // is the string long enough to be valid command >= 3
      cmd = instr.substring(0,3); // extract the lower 3 characters

     // Serial1.print("The input cmd is :"); // diagnostic display of input command
     // Serial1.println(cmd);

      if(cmd.equals("H1O")) {  // process heater loop on commands
        Serial1.println("H1ON");
        htrstat[0] = true;
      }
      else if (cmd.equals("H2O")) {
        Serial1.println("H2ON");
        htrstat[1] = true;
      }
      else if (cmd.equals("H3O")) {
        Serial1.println("H3ON");
        htrstat[2] = true;
      }
      else if (cmd.equals("H4O")) {
        Serial1.println("H4ON");
        htrstat[3] = true;
      }

      else if (cmd.equals("H1F")) { // process heater loop off commands
        Serial1.println("H1OFF");
        htrstat[0] = false;
      }
      else if (cmd.equals("H2F")) {
        Serial1.println("H2OFF");
        htrstat[1] = false;
      }
      else if (cmd.equals("H3F")) {
        Serial1.println("H3OFF");
        htrstat[2] = false;
      }
      else if (cmd.equals("H4F")) {
        Serial1.println("H4OFF");
        htrstat[3] = false;
      }

      else if (cmd.equals("HS1")) { // process heater loop status comands
        //Serial1.println("Valid cmd of HEATER 1 STATUS");
        if (htrstat[0] == 1) { Serial1.println("SH1ON"); }
        else { Serial1.println("SH1OFF"); }
      }
      else if (cmd.equals("HS2")) {
        //Serial1.println("Valid cmd of HEATER 2 STATUS");
        if (htrstat[1] == 1) { Serial1.println("SH2ON"); }
        else { Serial1.println("SH2OFF"); }
      }
      else if (cmd.equals("HS3")) {
        //Serial1.println("Valid cmd of HEATER 3 STATUS");
        if (htrstat[2] == 1) { Serial1.println("SH3ON"); }
        else { Serial1.println("SH3OFF"); }
      }
      else if (cmd.equals("HS4")) {
        //Serial1.println("Valid cmd of HEATER 4 STATUS");
        if (htrstat[3] == 1) { Serial1.println("SH4ON"); }
        else { Serial1.println("SH4OFF"); }
      }

      else if  (cmd.equals("ST1")) { // process set heater loop temperature commands
        val = instr.substring(3,tlen);
        val.trim(); // get rid of non printing characters
        value = val.toFloat(); // set to 0.0 if not valid floating point munber
        Serial1.print("ST1 ");
        Serial1.println(value,1);
        htrtemp[0] = value; // set the set point of the loop
      }
      else if (cmd.equals("ST2")) {
        val = instr.substring(3,tlen);
        val.trim();
        value = val.toFloat();
        Serial1.print("ST2 ");
        Serial1.println(value,1);
        htrtemp[1] = value;
      }
      else if (cmd.equals("ST3")) {
        val = instr.substring(3,tlen);
        val.trim();
        value = val.toFloat();
        Serial1.print("ST3 ");
        Serial1.println(value,1);
        htrtemp[2] = value;
      }
      else if (cmd.equals("ST4")) {
        val = instr.substring(3,tlen);
        val.trim();
        value = val.toFloat();
        Serial1.print("ST4 ");
        Serial1.println(value,1);
        htrtemp[3] = value;
      }

      else if (cmd.equals("RT1")) { // process read temperature commends
        Serial1.print("T1 ");
        Serial1.println(temp[0],1);
      }
      else if (cmd.equals("RT2")) {
        Serial1.print("T2 ");
        Serial1.println(temp[1],1);
      }
      else if (cmd.equals("RT3")) {
        Serial1.print("T3 ");
        Serial1.println(temp[2],1);
      }
      else if (cmd.equals("RT4")) {
        Serial1.print("T4 ");
        Serial1.println(temp[3],1);
      }
      else if (cmd.equals("RT5")) {
        Serial1.print("T5 ");
        Serial1.println(temp[4],1);
      }
      else if (cmd.equals("RT6")) {
        Serial1.print("T6 ");
        Serial1.println(temp[5],1);
      }
      else if (cmd.equals("RT7")) {
        Serial1.print("T7 ");
        Serial1.println(temp[6],1);
      }
      else if (cmd.equals("RT8")) {
        Serial1.print("T8 ");
        Serial1.println(temp[7],1);
      }
      else if (cmd.equals("RT9")) {
        Serial1.print("T9 ");
        Serial1.println(temp[8],1);
      }
      else if (cmd.equals("RTA")) {
        Serial1.print("TA ");
        Serial1.println(temp[9],1);
      }
      else if (cmd.equals("RTB")) {
        Serial1.print("TB ");
        Serial1.println(temp[10],1);
      }
      else if (cmd.equals("RTC")) {
        Serial1.print("TC ");
        Serial1.println(temp[11],1);
      }
      else if (cmd.equals("RHW")) { // Read HWAS status line, HIGH = armed, no event. LOW = no power or event detected
        level = digitalRead(22);
        if (level >= 0.5){ Serial1.println("HWOK"); }
        else { Serial1.println("HWEV"); }
      }
      else if (cmd.equals("RSH")) { // Generate 1 second reset pulse for HWAS
        digitalWrite(23,HIGH);
        delay(1000);
        digitalWrite(23,LOW);
        Serial1.println("RSTHWAS");
      }
      else if (cmd.equals("RL1")) { // Read limit switch 1. If switch open return "RL1OFF", if closed return "RL1ON"
        level = digitalRead(24);
        if (level >= 0.5){ Serial1.println("RL1OFF"); }
        else { Serial1.println("RL1ON"); }
       }
      else if (cmd.equals("RL2")) { // Read limit switch 2. If switch open return "RL2OFF", if closed return "RL2ON"
        level = digitalRead(25);
        if (level >= 0.5){ Serial1.println("RL2OFF"); }
        else { Serial1.println("RL2ON"); }
      }

      else {
        // unrecognized command
        Serial1.print("ERROR ");
        Serial1.print(char(34));
        Serial1.print(instr);
        Serial1.println(char(34));
      }
      delay(10);
    }
    else {
      // Input too short
        Serial1.print("ERROR ");
        Serial1.print(char(34));
        Serial1.print(instr);
        Serial1.println(char(34));
    }
  }
  else {
    //Serial1.println("No input");
    delay(10);
  }

}

void ProcTemp(int tc) { // process a temperature loop channel

  if (htrstat[tc] == true) { // is loop enabled?
    if (temp[tc] > (htrtemp[tc]+1.0)) { // is loop temp greater than set point + 1
      digitalWrite(RlyPin[tc], LOW); // turn off heater
    }
    if (temp[tc] < (htrtemp[tc]-1.0)) { // is loop less than set point - 1
      digitalWrite(RlyPin[tc], HIGH); // turn on heater
    }
  }
  else { // loop not enabled turn off heater
     digitalWrite(RlyPin[tc], LOW);
   }
}

void convtemp(int tchan) { // convert a/d counts to corrected temperature deg C

  temp[tchan] = (((0.0049*float(analogRead(tchan)))-2.6632)/0.0261)+tcal[tchan];
}

