#include <Arduino.h>
#include "Wire.h"
#include "LM75/LM75.h"

#define address 0x48

LM75 sensor;

void setup(void)
{
	Wire.begin();
	Serial.begin(9600);

	Serial.println("Temperatures will be displayed every second:");

	sensor.tos(80); // set at 47.5'C
	Serial.print("Tos set at ");
	Serial.print(sensor.tos());
	Serial.println(" C");

	// Thyst Set-point
	sensor.thyst(75); // set at 42'C
	Serial.print("Thyst set at ");
	Serial.print(sensor.thyst());
	Serial.println(" C");
}
void loop()
{
	//  Wire.beginTransmission(address);
	//  Wire.write(0x00);
	//  Wire.requestFrom(address, 2); //was 1
	//  float temperature;
	//  if (Wire.available()) {
	//     temperature = Wire.read();
	//  }
	//  Wire.endTransmission();
	//  Serial.println("-----------");
	//  Serial.print(temperature);
	//  Serial.println(" C");
	//

	// get temperature from sensor
	Serial.print("Current temp: ");
	Serial.print(sensor.temp());
	Serial.println(" C");


	delay(1000);


}
