/***************************************************************************//**
 *   @file   AD7291.c
 *   @brief  Implementation of AD7291 Driver.
 *   @author DBogdan (dragos.bogdan@analog.com)
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 590
*******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/
#include "AD7291.h"

/***************************************************************************//**
 * @brief Initializes the I2C communication peripheral and configures
 *        the device.
 *
 * @return status - The result of the I2C initialization.
 *                  Example: 0 - Initialization failed;
 *                           1 - Initialization succeeded.
*******************************************************************************/
unsigned char AD7291_Init(void)
{
    unsigned char status = 0;
    
    status = I2C_Init();
    /* It is recommended to enable AD7291_COMMAND_DELAY feature for
       normal operation. */
    AD7291_SetRegisterValue(AD7291_REG_COMMAND, AD7291_COMMAND_DELAY);
    
    return status;
}

/***************************************************************************//**
 * @brief Writes data into a register.
 *
 * @param registerAddress - Address of the register.
 * @param registerValue - Value of the register.
 *
 * @return None.
*******************************************************************************/
void AD7291_SetRegisterValue(unsigned char registerAddress,
                             unsigned short registerValue)
{
    unsigned char sendData[3] = {0, 0, 0};
    
    sendData[0] = registerAddress;
    sendData[1] = ((registerValue & 0xFF00) >> 8);
    sendData[2] = ((registerValue & 0x00FF));
    I2C_Write(AD7291_ADDRESS,
              sendData,
              3,
              1);
}

/***************************************************************************//**
 * @brief Performs a number of reads from a register.
 *
 * @param registerAddress - Address of the register.
 * @param registerValue - Pointer to a buffer that will store the received data.
 * @param readsNumber - Number of reads that will be performed.
 *
 * @return None.
*******************************************************************************/
void AD7291_GetRegisterValue(unsigned char registerAddress,
                             unsigned short* registerValue,
                             unsigned char readsNumber)
{
    unsigned char receiveBuffer[16] = {0, };
    unsigned char read = 0;
    
    I2C_Write(AD7291_ADDRESS,
              (unsigned char*)&registerAddress,
              1,
              1);
    I2C_Read(AD7291_ADDRESS,
             receiveBuffer,
             (2 * readsNumber),
             1);
    for(read = 0; read < readsNumber; read++)
    {
        registerValue[read] = ((unsigned short)receiveBuffer[read * 2] << 8) +
                              receiveBuffer[read * 2 + 1];
    }
}

/***************************************************************************//**
 * @brief Reads the temperature data and converts the result in degrees Celsius.
 *
 * @return temperature - Temperature in degrees Celsius.
*******************************************************************************/
float AD7291_GetDegreesTemp(void)
{
    unsigned short commandValue = 0;
    unsigned short tsenseReg = 0;
	float temperature = 0x0;
    
    AD7291_GetRegisterValue(AD7291_REG_COMMAND,
                            (unsigned short*)&commandValue,
                            1);
    commandValue |= AD7291_COMMAND_TSENSE;
    AD7291_SetRegisterValue(AD7291_REG_COMMAND, commandValue);
	AD7291_GetRegisterValue(AD7291_REG_T_SENSE,
                            (unsigned short*)&tsenseReg,
                            1);
	tsenseReg = (tsenseReg & 0x0FFF);
	if(tsenseReg & 0x800)
	{
		/* Negative Temperature = (4096 - ADC Code) / 4 */
		tsenseReg = (4096 - tsenseReg);
        temperature = tsenseReg;
        temperature = (temperature / 4);
		temperature = temperature * (-1);
	}
	else
	{
		/* Positive Temperature = ADC Code / 4 */
        temperature = tsenseReg;
		temperature = temperature / 4;
	}

	return temperature;
}

/***************************************************************************//**
 * @brief Reads the voltage conversion results for the enabled channels.
 *
 * @param data - Pointer to a buffer that will store the voltage conversion
 *               results.
 *               Example: data[0] - channel 0 data;
 *                        ...
 *                        data[7] - channel 7 data.
 *
 * @return None.
*******************************************************************************/
void AD7291_GetVoltageConversionResults(unsigned short* data)
{
    unsigned short commandValue = 0;
	unsigned char mask = 0x80;
    unsigned char channelsNumber = 0;
    unsigned short voltageData[16] = {0, };
    unsigned char channel = 0;
    unsigned char channelAddress = 0;
    
    AD7291_GetRegisterValue(AD7291_REG_COMMAND,
                            (unsigned short*)&commandValue,
                            1);
    commandValue = (commandValue & 0xFF00) >> 8;
    while(mask)
    {
        if(commandValue & mask)
        {
            channelsNumber++;
        }
        mask = mask >> 1;
    }

    if(channelsNumber)
    {
        AD7291_GetRegisterValue(AD7291_REG_VOLTAGE,
                                voltageData,
                                channelsNumber);
    }
    for(channel = 0; channel < channelsNumber; channel++)
    {
        channelAddress = (voltageData[channel] & 0xF000) >> 12;
        data[channelAddress] = (voltageData[channel] & 0x0FFF);
    }
}
