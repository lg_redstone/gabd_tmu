#include <Arduino.h>
#include "AD7291.h"

#define baud_rate 115200


float readInternalTemp();
int readVoltage();

void setup(void)
{
	Wire.begin();
	delay(500);
	Wire.setClock(400000L);

	Wire.beginTransmission(AD7291_ADDRESS);
	Wire.write(AD7291_REG_COMMAND);
	Wire.write(0x80);					//Write MSB
	Wire.write(0x21);					//Write LSB
	Wire.endTransmission();
	delay(50);

	Serial.begin(baud_rate);

	Serial.println("Starting at "+baud_rate);

}
void loop()
{
	int number = 0;

	number = readVoltage();
	Serial.println(number);


	//
	//

}


float readInternalTemp(){
	unsigned short lowByte = 0;
	unsigned short highByte = 0;
	unsigned short output = 0;
	float temperature = 0;
	Wire.beginTransmission(AD7291_ADDRESS);
	Wire.write(AD7291_REG_T_SENSE);
	////	Wire.write(AD7291_REG_T_AVERAGE);
	Wire.endTransmission();
	delay(5);

	Wire.requestFrom(AD7291_ADDRESS,2);

	highByte = Wire.read(); 	// Reads the data from the register
	lowByte = Wire.read();

	output = (highByte << 8) | lowByte;
	//	Serial.print("Total byte: ");
	//	Serial.println(output,BIN);

	output = (output & 0x0FFF);
	//	Serial.print("12 bytes:   ");
	//	Serial.println(output,BIN);

	//	delay(1000);


	if(output & 0x800)
	{
		/* Negative Temperature = (4096 - ADC Code) / 4 */
		output = (4096 - output);
		temperature = output;
		temperature = (temperature / 4);
		temperature = temperature * (-1);
	}
	else
	{
		/* Positive Temperature = ADC Code / 4 */
		temperature = output;
		temperature = temperature / 4;
	}

	return temperature;
}
int readVoltage(){

	unsigned short lowByte = 0;
	unsigned short highByte = 0;
	unsigned short output = 0;

	Wire.beginTransmission(AD7291_ADDRESS);
	Wire.write(AD7291_REG_VOLTAGE);
	Wire.endTransmission();
	delay(5);

	Wire.requestFrom(AD7291_ADDRESS,2);
	highByte = Wire.read(); 	// Reads the data from the register
	lowByte = Wire.read();

	output = (highByte << 8) | lowByte;
		Serial.print("Total byte: ");
		Serial.println(output,BIN);

	output = (output & 0x0FFF);
//	output = ((output*2.5)/4096);
		Serial.print("12 bytes:   ");
		Serial.println(output,BIN);

	return output;

	//	delay(1000);


}

