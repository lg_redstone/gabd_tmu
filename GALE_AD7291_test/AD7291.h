/***************************************************************************//**
 *   @file   AD7291.h
 *   @brief  Header file of AD7291 Driver.
 *   @author DBogdan (dragos.bogdan@analog.com)
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 590
*******************************************************************************/
#ifndef _AD7291_H_
#define _AD7291_H_

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/
#include "Communication.h"

/******************************************************************************/
/* AD7291                                                                     */
/******************************************************************************/

/* Slave Address */

//#define AD7291_ADDRESS                  0x20    // AS0 = H; AS1 = H.
//#define AD7291_ADDRESS                  0x22    // AS0 = H; AS1 = NC.
//#define AD7291_ADDRESS                  0x23    // AS0 = H; AS1 = L.
//#define AD7291_ADDRESS                  0x28    // AS0 = NC; AS1 = H.
#define AD7291_ADDRESS                  0x2A    // AS0 = NC; AS1 = NC.
//#define AD7291_ADDRESS                  0x2B    // AS0 = NC; AS1 = L.
//#define AD7291_ADDRESS                  0x2C    // AS0 = L; AS1 = H.
//#define AD7291_ADDRESS                  0x2E    // AS0 = L; AS1 = NC.
//#define AD7291_ADDRESS                  0x2F    // AS0 = L; AS1 = L.

/* AD7291 Registers Definition */
#define AD7291_REG_COMMAND				0x00
#define AD7291_REG_VOLTAGE				0x01
#define AD7291_REG_T_SENSE				0x02
#define AD7291_REG_T_AVERAGE			0x03
#define AD7291_REG_CH0_DATA_HIGH		0x04
#define AD7291_REG_CH0_DATA_LOW			0x05
#define AD7291_REG_CH0_HYST				0x06
#define AD7291_REG_CH1_DATA_HIGH		0x07
#define AD7291_REG_CH1_DATA_LOW			0x08
#define AD7291_REG_CH1_HYST				0x09
#define AD7291_REG_CH2_DATA_HIGH		0x0A
#define AD7291_REG_CH2_DATA_LOW			0x0B
#define AD7291_REG_CH2_HYST				0x0C
#define AD7291_REG_CH3_DATA_HIGH		0x0D
#define AD7291_REG_CH3_DATA_LOW			0x0E
#define AD7291_REG_CH3_HYST				0x0F
#define AD7291_REG_CH4_DATA_HIGH		0x10
#define AD7291_REG_CH4_DATA_LOW			0x11
#define AD7291_REG_CH4_HYST				0x12
#define AD7291_REG_CH5_DATA_HIGH		0x13
#define AD7291_REG_CH5_DATA_LOW			0x14
#define AD7291_REG_CH5_HYST				0x15
#define AD7291_REG_CH6_DATA_HIGH		0x16
#define AD7291_REG_CH6_DATA_LOW			0x17
#define AD7291_REG_CH6_HYST				0x18
#define AD7291_REG_CH7_DATA_HIGH		0x19
#define AD7291_REG_CH7_DATA_LOW			0x1A
#define AD7291_REG_CH7_HYST				0x2B
#define AD7291_REG_T_SENSE_HIGH			0x1C
#define AD7291_REG_T_SENSE_LOW			0x1D
#define AD7291_REG_T_SENSE_HYST			0x1E
#define AD7291_REG_VOLTAGE_ALERT_STATUS	0x1F
#define AD7291_REG_T_ALERT_STATUS		0x20

/* AD7291_REG_COMMAND Definition */
#define AD7291_COMMAND_CH(x)            (1 << (15 - x))
#define AD7291_COMMAND_TSENSE           (1 << 7)
#define AD7291_COMMAND_DELAY            (1 << 5)
#define AD7291_COMMAND_EXT_REF          (1 << 4)
#define AD7291_COMMAND_ALERT_POL_LOW    (1 << 3)
#define AD7291_COMMAND_ALERT_POL_HIGH   (0 << 3)
#define AD7291_COMMAND_CLR_ALERT        (1 << 2)
#define AD7291_COMMAND_RESET            (1 << 1)
#define AD7291_COMMAND_AUTOCYCLE        (1 << 0)

/******************************************************************************/
/* Functions Prototypes                                                       */
/******************************************************************************/

/* Initializes the I2C communication peripheral and configures AD7291. */
unsigned char AD7291_Init(void);

/* Writes data into a register. */
void AD7291_SetRegisterValue(unsigned char registerAddress,
                             unsigned short registerValue);

/* Performs a number of reads from a register. */
void AD7291_GetRegisterValue(unsigned char registerAddress,
                             unsigned short* registerValue,
                             unsigned char readsNumber);

/* Reads the temperature data and converts the result in degrees Celsius. */
float AD7291_GetDegreesTemp(void);

/* Reads the voltage conversion results for the enabled channels. */
void AD7291_GetVoltageConversionResults(unsigned short* data);

#endif // _AD7291_H
